import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
 /* eslint-disable */

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faUserSecret,
	faTimes,
	faEdit,
	faPlus,
	faSync,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
	faUserSecret,
	faTimes,
	faEdit,
	faPlus,
	faSync,
	)

Vue.component('font-awesome-icon', FontAwesomeIcon)

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)

import VueSweetalert2 from 'vue-sweetalert2'
 
import 'sweetalert2/dist/sweetalert2.min.css'
 
Vue.use(VueSweetalert2)

import VueMoment from 'vue-moment'
import moment from 'moment-timezone' 
 
Vue.use(VueMoment, {
    moment,
})

Vue.use(require('@/plugins/index.js'))

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
